#!/usr/bin/env python3
################################################################################
# by gibbonjoyeux
################################################################################
# Description:
#  "ant" is a program which aims to ease C or C++ development projects,
#  to help developers build and run their projects and to create a simple
#  environnement.
#  For more information, run "ant help"
################################################################################

################################################################################
### MODULES
################################################################################

import	os
import	re
import	sys
import	subprocess as sub

################################################################################
### GLOBALS
################################################################################

g_config = {}
g_cwd = os.getcwd()
g_path_ant = None
g_path_project = None
g_objects = []
g_project_name = None
g_flags = " -Wall -Wextra -Werror -g "
g_conf_format = {
    "name": None,
    "type": ["bin", "lib"],
    "compiler": ["clang", "gcc", "g++", "clang++"],
    "strict": ["true", "false"],
    "optimization": ["none", "0", "1", "2", "3", "s", "fast"],
    "flags_project": None,
    "flags_file": None
}
g_conf_default = {
    "name": "a.out",
    "type": "bin",
    "compiler": "gcc",
    "strict": "true",
    "optimization": "none",
    "flags_project": "",
    "flags_file": ""
}
g_basic_usage = "ant COMMAND [ARGUMENTS]..."
g_description = \
""""ant" is a tool which aims to ease C and C++ development
projects, to help developers build and run their projects and to create a
simpler environnement."""
g_usages = [
    {
        "name": "build",
        "parameters": "[PATH]",
        "description": "Compiles project",
        "full_description": """
Build compiles project source files to build project program or library.
Sources files must be present in "src/" directory (the search is made
recursively. It means that you can have directories into "src/".).
Sources files are compiled with header files present into the "inc/" directory.
A simple compilation does not need any header file ("inc/" can be empty or
innexistant).


The "ant/" directory (optional) can contain 2 files. "config" and "lib".

"ant/config" is a the project configuration file. It's the file where you define
the project name, type, compiler, optimizations etc.
The config file is composed of a list of key/value, here's an example:
  name = my_program # MY PROGRAM NAME
  type = bin        # THE TYPE OF THE PROJECT, HERE, IT'S DEFINED AS A BIN
  strict = true     # ACTIVE STRICT MODE FOR COMPILATION
There is no required key. Here are the default values:
  name = a.out
  type = bin          # bin | lib
  compiler = gcc      # gcc | clang | g++ | clang++
  strict = true       # true | false
  optimization = none # none | 0 | 1 | 2 | 3 | s | fast
  flags_file =
  flags_project =

"ant/lib" is the file which defines every used external library path, here's an
example:
  :my_lib_string
  gitlab.com/cactusfluo/my_lib_print
  ./lib/my_lib_number
  /home/cactus/my_libs/my_lib_complex_numbers
Here are the different formats a path can take:
- start with ":" -> ant will look for the lib into "lib/" (in project root)
- start with "./" or "../" or "/" -> relative or absolute path 
- format like "host.com/user/project" -> ant will look for the lib into the ant
  environnement sources. (for more info, run "ant help get")


if PATH is specified, ant will build the project present at PATH.
"""
    },
    {
        "name": "run",
        "parameters": "[ARGUMENTS]...",
        "description": "Compiles and run current project",
        "full_description":"""
Run compiles the current project just like "ant build" does.
Once the project compiled, it will run it with given ARGUMENTS.
example: (with a.out as default program name)
  $ ant run 1 2 3
  un deux trois
  $ ./a.out 1 2 3
  un deux trois
"""
    },
    {
        "name": "clean",
        "parameters": "[PATH]",
        "description": "Cleans project",
        "full_description":"""
Clean cleans the project directory. It removes object files (present into "obj/"
directory) and the project program or library file.
"""
    },
    {
        "name": "state",
        "parameters": "",
        "description": "Prints project state",
        "full_description": """
State prints a simple overview of the project state:
  - libraries (and if they are reachable or not)
  - includes (and if they had been modified since last compilation)
  - sources files (and if they had been modified since last compilation)
"""
    },
    {
        "name": "init",
        "parameters": "[PATH] [-t template] [-b|-l]",
        "description": "Initializes or creates new project directory",
        "full_description":"""
Init initializes or creates a new project directory.
Optionnal flags:
  -b, --bin              The project will be created as a bin
  -l, --lib              The project will be created as a lib
  -t, --template path    The project will be created from an existant template

By default, "ant init" creates "src/", "inc/" and "ant/" directories with a
basic "ant/config" and a simple ".gitignore" file.
--bin and --lib allows you to define the project type. (by default, the project
is a bin).

You can also use homemade templates (present at ${ANTPATH}/templates/) to
create your new project. In this way, it will copy every file from the template
directory to the project directory.
template variables:
  ${NAME}  accessible from "ant/config" and ".gitignore", will be replaced by
    the project name.
  ${TYPE}  accessible from "ant/config", will be replaced by the project type.

If PATH is specified, a new directory will be created and the project will be
placed here.
"""
    },
    {
        "name": "get",
        "parameters": "URL",
        "description": "Get or updates project",
        "full_description":"""
Get get or updates project from it git URL.
The url must be formatted as followed: "host.domain/user/project", here's an
example:
  $ ant get gitlab.com/cactusfluo/ct_lib

The project will be downloaded into the ant environnement at ${ANTPATH}.
If the environnement variable ${ANTPATH} is not defined, ant will define it as
"${HOME}/ant/".
Once downloaded, the project will be found at
"${ANTPATH}/src/host.domain/user/project".
This ant environnement allows you to easily import your libraries from every
project on your computer. (for more info on library, run "ant help build")
"""
    },
    {
        "name": "depend",
        "parameters": "",
        "description": "Load dependencies",
        "full_description": """
Depend loads the project dependencies from the "ant/lib" file. Every lib
formatted like "host.domain/user/project" will be loaded as it would be with
"ant get". (For more info on library, run "ant help get")
"""
    },
    {
        "name": "list",
        "parameters": "[-l|-t]",
        "description": "Lists projects presents into ${ANTPATH}/src",
        "full_description":"""
List lists projects present into ${ANTPATH}/src (by default, ${HOME}/ant/src).
Optionnal flags:
  -l, --list    Lists projects as a simple list
  -t, --tree    Lists projects as a tree
"""
    },
    {
        "name": "help",
        "parameters": "[COMMAND]",
        "description": "Prints basic usage or command description",
        "full_description":"""
Help prints basic usage of the "ant" tool.
If COMMAND is specified, help will print the command description and usage.
"""
    }
]

################################################################################
### FUNCTIONS
################################################################################

def			printf(format, *args):
    sys.stdout.write(format % args)

def			eprintf(format, *args):
    sys.stderr.write(format % args)

def			error(format, *args):
    sys.stderr.write(C_RED + "ERROR: " + C_NO + format % args + "\n")

##################################################
### BASICS
##################################################

def			set_colors():
    global C_RED, C_GREEN, C_YELLOW, C_BLUE, C_PINK, C_CYAN, C_NO
    ### IF OUTPUT IS TERMINAL
    if sys.stdout.isatty():
        C_RED = "\033[31;01m"
        C_GREEN = "\033[32;01m"
        C_YELLOW = "\033[33;01m"
        C_BLUE = "\033[34;01m"
        C_PINK = "\033[35;01m"
        C_CYAN = "\033[36;01m"
        C_NO = "\033[0m"
    ### IF OUTPUT IS PIPED/REDIRECTED
    else:
        C_RED = ""
        C_GREEN = ""
        C_YELLOW = ""
        C_BLUE = ""
        C_PINK = ""
        C_CYAN = ""
        C_NO = ""

##############################
### CONFIGURATION
##############################

def			load_conf(path):
    conf_file = path + "/ant/config"
    if not os.path.isfile(conf_file):
        return dict(g_conf_default)
    last_key = None
    conf = dict(g_conf_default)
    with open(conf_file) as file:
        lines = file.readlines()
        for line in lines:
            line = re.sub(r"\s*(#.*){0,1}\n$", "", line)
            if len(line) == 0:
                continue
            # GET KEY / VALUE
            key = re.sub(r"\s*=.*$", "", line)
            value = re.sub(r"^.*=\s*", "", line)
            # APPEND VALUE TO PREVIOUS KEY
            if key == value:
                 value = conf[last_key] + " " + value
                 key = last_key
            # ERROR KEY
            if not key in g_conf_format:
                error("Unknown config key \"%s\"", key)
                sys.exit(1)
            # ERROR VALUE
            if g_conf_format[key] != None \
            and not value in g_conf_format[key]:
                error("Unknown config value \"%s\" for key \"%s\"", value,
                key)
                eprintf("Possible value of \"%s\" key: %s\n", key,
                " | ".join(g_conf_format[key]))
                sys.exit(1)
            # UPDATE KEY
            conf[key] = value
            last_key = key
    if conf["type"] == "lib":
        if conf["name"] == g_conf_default["name"]:
            conf["name"] = "lib.a"
        else:
            conf["name"] += ".a"
    return conf
    
##############################
### PATH
##############################

def			go_to_path_project():
    global	g_path_project

    prev_cwd = os.getcwd()
    cwd = prev_cwd
    ### GO BACK UNTIL CONFIG CAN BE FOUND
    while not os.path.isfile("ant/config"):
        if os.chdir("../"):
            error("Cannot move to project directory. Please check your rights")
            sys.exit(1)
        cwd = os.getcwd()
        if prev_cwd == cwd:
            error("Not a valid project directory.")
            sys.exit(1)
        prev_cwd = cwd
    g_path_project = cwd

def			load_path_ant():
    global	g_path_ant

    if g_path_ant:
        return
    g_path_ant = os.environ.get("ANTPATH")
    if g_path_ant == None or g_path_ant == "":
        home = os.environ.get("HOME")
        if home == None:
            error("ANTPATH or HOME must initialized into env to create the ant "
            + "environnement.")
        g_path_ant = home + "/ant"
    if not os.path.isdir(g_path_ant):
        process = sub.run(["mkdir", "-p", g_path_ant])
        if process.returncode != 0:
            error("Cannot create ant environnement. Please check your rights")
            sys.exit(1)

def			go_to_path_ant():
    if not g_path_ant:
        load_path_ant()
    os.chdir(g_path_ant)

##############################
### LOADING
##############################

def		load_sources(src_dir, to_compile=None, compiled=None, oldest=None):
    if to_compile == None:
        to_compile = []
    if compiled == None:
        compiled = []
    ### HANDLE OBJECT DIRECTORY
    src_dir += "/"
    obj_dir = re.sub(r"^src", "obj", src_dir)
    if (not os.path.isdir(obj_dir)):
        os.mkdir(obj_dir)
    #### HANDLE FILE
    files = os.listdir(src_dir)
    #print("sources", files, to_compile, compiled)
    for item in files:
        #print("sources -",  item)
        src_file = src_dir + item
        if ((os.path.isfile(src_file) and src_file[-2:] == ".c" or
            src_file[-4:] == ".cpp")):
            obj_file = obj_dir + re.sub(r"\.c(|pp)$", ".o", item)
            if os.path.isfile(obj_file):
                obj_creation_time = os.path.getmtime(obj_file)
                if not oldest or oldest > obj_creation_time:
                    oldest = obj_creation_time
                if os.path.getmtime(src_file) > obj_creation_time:
                    to_compile.append(src_file)
                else:
                    compiled.append(src_file)
            else:
                to_compile.append(src_file)
        elif (os.path.isdir(src_file)):
            _, _, oldest = load_sources(src_file, to_compile, compiled, oldest)
    #print("sources", to_compile, compiled)
    return to_compile, compiled, oldest

def			load_libraries():
    if not os.path.isfile("ant/lib"):
        return []
    load_path_ant()
    libraries = []
    with open("ant/lib") as file:
        lines = file.readlines()
        for line in lines:
            line = re.sub(r"#.*$", "", line)
            line = re.sub(r"\s", "", line)
            if line == "":
                continue
            line = re.sub(r"^:", "./lib/", line)
            if re.search(r"^(\.){0,2}/", line):
                lib = line
            else:
                lib = g_path_ant + "/src/" + line
            libraries.append(lib)
    return libraries

##################################################
### BUILD
##################################################

def			ant_build(args=[], sub_lib=False):
    def		check_force(libraries, oldest_obj):
        if oldest_obj == None:
            return True
        ### CHECK CONFIG FILE
        if os.path.isfile("ant/config"):
            if os.path.getmtime("ant/config") > oldest_obj:
                return True
        ### CHECK LIB FILE
        if os.path.isfile("ant/lib"):
            if os.path.getmtime("ant/lib") > oldest_obj:
                return True
        ### CHECK INCLUDES
        if os.path.isdir("inc"):
            for _, _, files in os.walk("inc"):
                for file in files:
                    if file[-2:] == ".h":
                        if os.path.getmtime("inc/" + file) > oldest_obj:
                            return True
        return False

    def		load_comp_str_file(conf, headers):
        opti = {
            "0": "-O0",
            "1": "-O1",
            "2": "-O2",
            "3": "-O3",
            "s": "-Os",
            "fast": "-Ofast"
        }
        comp = [conf["compiler"], "-I", "inc"]
        if conf["optimization"] != "none":
            comp.append(opti[conf["optimization"]])
        if len(conf["flags_file"]):
            comp.extend(conf["flags_file"].split())
        if conf["strict"] == "true":
            comp.extend(["-Wall", "-Wextra", "-Werror"])
        for header in headers:
            comp.extend(["-I", header])
        return comp

    ##############################
    ### COMPILATION
    ##############################
    
    def		get_compiled_obj(compiled, path):
        path += "/"
        obj_files = []
        for src in compiled:
            obj = re.sub(r"^src", "obj", re.sub(r"c(|pp)$", "o", src))
            obj_files.append(path + obj)
        return obj_files

    def		compile_sources(cmd_base, to_compile, project_name, path):
        sys.stdout.write("{}\n".format(project_name))
        obj_files = []
        i = 0
        i_max = len(to_compile)
        if i_max > 0:
            for src in to_compile:
                obj = re.sub(r"^src", "obj", re.sub(r"c(|pp)$", "o", src))
                obj_files.append(path + "/" + obj)
                cmd = cmd_base + ["-c", src, "-o", obj]
                process = sub.run(cmd, stderr=sub.PIPE)
                if process.returncode != 0:
                    ### PRINT COMPILATION ERROR
                    sys.stdout.flush()
                    error("Cannot compile %s file:", src)
                    sys.stderr.write(process.stderr.decode("utf-8") + "\n")
                    sys.exit(1)
                ### PRINT PROGRESS
                sys.stdout.write("[{:>3}%] {}\n".format(
                str(max(1, (100 * (i + 1)) // (i_max + 1))), src))
                i += 1
        sys.stdout.flush()
        return obj_files
    
    def		compile_project(cmd, conf):
        process = sub.run(cmd)
        if process.returncode != 0:
            error("Cannot compile project.")
            sys.exit(1)
        if conf["type"] == "lib":
            process = sub.run(["ranlib", conf["name"]])
            if process.returncode != 0:
                error("Cannot ranlib project.")
                sys.exit(1)
        sys.stdout.write("[100%] {}\n".format(conf["name"]))
        sys.stdout.flush()

    def         build(conf):
        force = False
        obj_list = []
        lib_list = []
        inc_list = []
        project_cwd = os.getcwd()
        ### LIBRARIES
        lib_path_list = load_libraries()
        for lib_path in lib_path_list:
            if not os.path.isdir(lib_path):
                error("Cannot find \"%s\" project.", lib_path)
                sys.exit(1)
            os.chdir(lib_path)
            lib_conf = load_conf(".")
            lib_force, new_obj_list, new_inc_list = build(lib_conf)
            if lib_force == True:
                force = True
            obj_list.extend(new_obj_list)
            inc_list.extend(new_inc_list)
            lib_list.append(lib_path + "/" + lib_conf["name"])
            os.chdir(project_cwd)
        ### ADD INCLUDES
        inc_list.append(project_cwd + "/inc")
        ### COMPILE SOURCES
        src_to_compile, src_compiled, oldest_obj = load_sources("src")
        if len(src_to_compile) == 0 and len(src_compiled) == 0:
            error("Build requires at least one source file.")
            sys.exit(1)
        if force == True or check_force(lib_path_list, oldest_obj) == True:
            force = True
            src_to_compile += src_compiled
            src_compiled = []
        if conf["type"] == "bin":
            obj_list = []
        obj_list += get_compiled_obj(src_compiled, project_cwd)
        cmd_file = load_comp_str_file(conf, inc_list)
        obj_list += compile_sources(cmd_file, src_to_compile, conf["name"], project_cwd)
        ### COMPILE PROJECT
        if conf["type"] == "lib":
            cmd = ["ar", "rc", conf["name"]] + obj_list
        else:
            cmd = [conf["compiler"], "-o", conf["name"]] + obj_list + lib_list
            if len(conf["flags_project"]):
                cmd.extend(conf["flags_project"].split())
        compile_project(cmd, conf)
        return force, obj_list, inc_list

    ##############################
    ### BUILD MAIN
    ##############################
    
    ### CHECK PARAMETERS
    if len(args) > 1:
        error("Build requires a maximum of one parameter.")
        usage("build")
        sys.exit(1)
    if len(args) == 1:
        if not os.path.isdir(args[0]) or os.chdir(args[0]):
            error("Parameter is not a valid directory \"{}\"".format(args[0]))
            usage("build")
            sys.exit(1)
    ### GET PROJECT INFORMATIONS
    go_to_path_project()
    conf = load_conf(".")
    ### BUILD PROJECT
    build(conf)
    return conf["name"]

##################################################
### RUN
##################################################

def			ant_run(args):
    program = "./" + ant_build()
    if args:
        args = [program] + args
    else:
        args = [program]
    try:
        os.execv(program, [program] + args)
    except:
        error("Cannot run project.")
        sys.exit(1)

##################################################
### CLEAN
##################################################

def			ant_clean(args):
    def		clean_project(path):
        if not os.path.isdir(path) or os.chdir(path):
            error("Parameter is not a valid directory \"{}\"".format(path))
            usage("clean")
            sys.exit(1)
        go_to_path_project()
        conf = load_conf(".")
        process = sub.run(["rm", "-rf", "obj", conf["name"]])
        if process.returncode != 0:
            error("Cannot remove object files. Please check your rights.")
            sys.exit(1)
        print("[{}]".format(conf["name"]))

    ### CLEAN CURRENT PROJECT
    if len(args) == 0:
        clean_project(".")
    ### CLEAN LISTED PROJECTS
    else:
        pwd = os.getcwd()
        for path in args:
            clean_project(path)
            if os.chdir(pwd):
                error("Cannot clean next projects. Please check your rights.")
                sys.exit(1)

##################################################
### INIT
##################################################

def				ant_init(args):
    ##############################
    ### INIT FUNCTIONS
    ##############################
    def			load_project_info():
        project_name = None
        project_type = None
        project_template = None
        i = 0
        length = len(args)
        while i < length:
            arg = args[i]
            ### INIT FROM TEMPLATE
            if arg == "-t" or arg == "--template":
                if project_template:
                    error("Project template has already been defined to \"{}\"." \
                    .format(project_template))
                    usage("init")
                    sys.exit(1)
                i += 1
                if i == length:
                    error("Missing template name.")
                    usage("init")
                    sys.exit(1)
                project_template = args[i]
            ### INIT BIN
            elif arg == "-b" or arg == "--bin":
                if project_type:
                    error("Project type has already been defined to \"{}\"." \
                    .format(project_type))
                    usage("init")
                    sys.exit(1)
                project_type = "bin"
            ### INIT LIB
            elif arg == "-l" or arg == "--lib":
                if project_type:
                    error("Project type has already been defined to \"{}\"." \
                    .format(project_type))
                    usage("init")
                    sys.exit(1)
                project_type = "lib"
            ### PROJECT NAME
            else:
                if project_name:
                    error("Project name has already been defined to \"{}\"." \
                    .format(project_name))
                    usage("init")
                    sys.exit(1)
                project_name = arg
            i += 1
        if project_name == None:
            project_name = "."
        if project_type == None:
            project_type = "bin"
        return project_name, project_type, project_template

    def			create_with_template():
        load_path_ant()
        template_path = g_path_ant + "/templates/" + project_template
        if not os.path.isdir(template_path):
            error("Innexistant template \"{}\". Templates can be found at {}" \
            .format(project_template, "${ANTPATH}/templates/"))
            usage("init")
            sys.exit(1)
        ### IMPORT TEMPLATE FILES
        command = ["cp", "-r"]
        template_files = os.listdir(template_path)
        for template_file in template_files:
            command.append(template_path + "/" + template_file)
        command.append(".")
        process = sub.run(command)
        if process.returncode != 0:
            error("Cannot import template files. Please check your rights")
            sys.exit(1)
        ### CONFIGURATION VARIABLES
        if os.path.isfile("ant/config"):
            with open("ant/config", "r") as file:
                content = file.read()
                content = re.sub(r"\$\{NAME\}", project_name, content)
                content = re.sub(r"\$\{TYPE\}", project_type, content)
            with open("ant/config", "w") as file:
                file.write(content)
        ### ".gitconfig" FILE
        if os.path.isfile(".gitignore"):
            with open(".gitignore", "r") as file:
                content = file.read()
                content = re.sub(r"\$\{NAME\}", project_name, content)
            with open(".gitignore", "w") as file:
                file.write(content)

    def			create_without_template():
        ### CREATE DIRECTORIES
        process = sub.run(["mkdir", "-p", "src", "inc", "ant"])
        if process.returncode != 0:
            error("Cannot create src/, inc/ and ant/ directories. Please check your rights")
            sys.exit(1)
        ### CREATE CONF FILE
        conf = dict(g_conf_default)
        conf["name"] = os.path.basename(os.getcwd())
        conf["type"] = project_type
        with open("ant/config", "w") as conf_file:
            for key, value in conf.items():
                if value == None:
                    value = ""
                conf_file.write("{:13} = {:7}".format(key, value))
                if g_conf_format[key]:
                    comment = "# {}\n".format(" | ".join(g_conf_format[key]))
                else:
                    comment = "\n"
                conf_file.write(comment)
        with open(".gitignore", "w") as git_conf_file:
            git_conf_file.write("{}\nobj".format(project_name))

    ##############################
    ### INIT MAIN
    ##############################
    ### LOAD PROJECT INFORMATION
    project_name, project_type, project_template = load_project_info()
    ### GO TO PROJECT PATH
    if not os.path.isdir(project_name):
        process = sub.run(["mkdir", "-p", project_name])
        if process.returncode != 0:
            error("Cannot create project directory. Please check your rights")
            sys.exit(1)
    if os.chdir(project_name):
        error("Cannot go into project directory. Please check your rights")
        sys.exit(1)
    project_name = os.path.basename(os.getcwd())
    ### CREATE PROJECT DIRECTORY
    if project_template:
        create_with_template()
    else:
        create_without_template()

##################################################
### GET
##################################################

def			ant_get(args):
    length = len(args)
    if length == 0:
        error("get requires at least one parameter.")
        usage("get")
        sys.exit(1)
    for project in args:
        print("Getting \"{}\" project.".format(project))
        go_to_path_ant()
        url = re.search(r"([\w\-\.]+/){2}[\w\-\.]+", project)
        if not url:
            error("Given url does not have a correct format.")
            usage("get")
            sys.exit(1)
        url = url.group(0)
        full_url = "https://" + url + ".git"
        if not os.path.isdir("src"):
            os.mkdir("src")
        os.chdir("src")
        if os.path.isdir(url):
            os.chdir(url)
            process = sub.run(["git", "pull"])
            if process.returncode != 0:
                error("Cannot update project. Please check the url.")
                sys.exit(1)
        else:
            process = sub.run(["mkdir", "-p", url])
            if process.returncode != 0:
                error("Cannot create project directory. Please check your rights.")
                sys.exit(1)
            process = sub.run(["git", "clone", full_url, url])
            if process.returncode != 0:
                error("Cannot clone project. Please check the url.")
                sys.exit(1)

##################################################
### DEPEND
##################################################

def			ant_depend(args):
    ### CHECK PARAMETERS
    if len(args) != 0:
        error("depend does not require any parameter.")
        usage("depend")
        sys.exit(1)
    ### CHECK DEPENDENCIES
    go_to_path_project()
    load_path_ant()
    re_path_ant = "^" + g_path_ant + "/src/"
    to_get = []
    libraries = load_libraries()
    for lib in libraries:
        lib_url = re.sub(re_path_ant, "", lib)
        if lib_url != lib:
            lib_url = re.search(r"([\w\-\.]+/){2}[\w\-\.]+", lib_url)
            if lib_url:
                to_get.append(lib_url.group(0))
    if len(to_get) > 0:
        ant_get(to_get)

##################################################
### STATE
##################################################

def			ant_state(args):
	##############################
    ### LIST FUNCTIONS
	##############################
    def		list_sources(path, count_total=0, count_modified=0):
        path = path + "/"
        files = os.listdir(path)
        files.sort()
        dirs = []
        ### LOOP ON FILES
        for file_name in files:
            src_path = path + file_name
            ### IF DIR
            if os.path.isdir(src_path):
                dirs.append(src_path)
            ### IF FILE
            elif os.path.isfile(src_path) and (src_path[-2:] == ".c" or
            src_path[-4:] == ".cpp"):
                obj_path = re.sub(r"\.c(|pp)$", ".o", src_path)
                obj_path = re.sub(r"^src", "obj", obj_path)
                ### IF MUST BE COMPILED
                if (not os.path.isfile(obj_path)
                or os.path.getmtime(src_path) > os.path.getmtime(obj_path)):
                    printf("  [{}x{}] {}".format(C_PINK, C_NO, C_YELLOW))
                    count_modified += 1
                ### IF UP TO DATE
                else:
                    printf("  [ ] {}".format(C_GREEN))
                print(src_path[4:] + C_NO)
                count_total += 1
        ### LOOP ON DIRECTORIES
        for dir_path in dirs:
            count_total, count_modified = list_sources(dir_path, \
            count_total, count_modified)
        return count_total, count_modified

    def		list_includes():
        count_total = 0
        count_modified = 0
        project_exists = os.path.isfile(g_project_name)
        if not os.path.isdir("inc"):
            print("")
            return
        includes = os.listdir("inc")
        includes.sort()
        for include in includes:
            if include[-2:] != ".h" and include[-4:] != ".hpp":
                continue
            count_total += 1
            inc_path = "inc/" + include
            ### IF MODIFIED
            if (not project_exists or os.path.getmtime(inc_path)
            > os.path.getmtime(g_project_name)):
                printf("  [{}x{}] {}".format(C_PINK, C_NO, C_YELLOW))
                count_modified += 1
            ### IF UP TO DATE
            else:
                printf("  [ ] {}".format(C_GREEN))
            print(include + C_NO)
        printf("  {}/{} modified ".format(count_modified, count_total))
        print("files\n" if count_modified > 1 else "file\n")

    def		list_libraries():
        count_total = 0
        count_notfound = 0
        if not os.path.isfile("ant/lib"):
            print("")
            return
        load_path_ant()
        with open("ant/lib") as file:
            lines = file.readlines()
            for line in lines:
                line = re.sub(r"#.*$", "", line)
                line = re.sub(r"\s", "", line)
                if line == "":
                    continue
                count_total += 1
                path = re.sub(r"^:", "./lib/", line)
                if re.search(r"^(\.){0,2}/", path):
                    lib = path
                else:
                    lib = g_path_ant + "/src/" + line
                if os.path.isdir(lib):
                    printf("  [ ] ")
                else:
                    printf("  [x] ")
                    count_notfound += 1
                print(line)
        printf("  {}/{} not found ".format(count_notfound, count_total))
        print("libraries\n" if count_notfound > 1 else "library\n")
            
    ##############################
    ### STATE MAIN
    ##############################
    global	g_project_name

    ### CHECK PARAMETERS
    if len(args) != 0:
        error("state does not require any parameter.")
        usage("state")
        sys.exit(1)
    ### LOAD PROJECT NAME
    go_to_path_project()
    conf = load_conf(".")
    g_project_name = conf["name"]
    print("[{}]\n".format(g_project_name))
    ### PRINT LIBRARIES
    print("{}LIBRARIES:{}".format(C_CYAN, C_NO))
    list_libraries()
    ### PRINT INCLUDES
    print("{}INCLUDES:{}".format(C_CYAN, C_NO))
    list_includes()
    print("{}SOURCES:{}".format(C_CYAN, C_NO))
    ### PRINT SOURCES
    count_total, count_modified = list_sources("src")
    printf("  {}/{} modified ".format(count_modified, count_total))
    print("files\n" if count_modified > 1 else "file\n")

##################################################
### LIST
##################################################

def			ant_list(args):
    if len(args) > 1:
        error("list requires a maximum of 1 parameter.")
        usage("list")
        sys.exit(1)
    elif len(args) == 0 or args[0] == "-l" or args[0] == "--list":
        mode = "list"
    elif args[0] == "-t" or args[0] == "--tree":
        mode = "tree"
    else:
        error("Unknows list flag \"{}\"".format(args[0]))
        usage("list")
        sys.exit(1)
    go_to_path_ant()
    hosts = os.listdir("src")
    for host in hosts:
        if mode == "tree":
            print(C_CYAN + host + C_NO + "/")
        host_path = "src/" + host
        users = os.listdir(host_path)
        for user in users:
            if mode == "tree":
                print(C_PINK + "    " + user + C_NO + "/")
            user_path = host_path + "/" + user
            projects = os.listdir(user_path)
            for project in projects:
                if mode == "tree":
                    print(C_GREEN + "        - " + C_YELLOW + project + C_NO)
                else:
                    print(user_path[4:] + "/" + project)

##################################################
### USAGE
##################################################

def			usage(command=None, full=False):
    if command == None:
        print(g_description)
        print("\n{}Usage: {}{}\n".format(C_YELLOW, C_NO, g_basic_usage))
        print("{}The commands are:\n".format(C_GREEN))
        for command in g_usages:
            print("{}{:10} {}{}".format(C_PINK, command["name"], C_NO, command["description"]))
        print("\nRun \"ant help [COMMAND]\" to get more information.")
    else:
        for use in g_usages:
            if use["name"] == command:
                print("{}Usage: {}ant {} {}".format(C_YELLOW, C_NO, command, \
				use["parameters"]))
                if full == True:
                    print(use["full_description"])
                else:
                    print("For more info, run \"ant help {}\".".format(command))
                return
        error("Innexistant ant command \"{}\"".format(command))
        usage()

################################################################################
### MAIN
################################################################################

##################################################
### BODY
##################################################

### SET COLOR FOR TERMINAL
set_colors()

### CHECK PYTHON VERSION
if sys.version_info < (3, 5):
    error("Ant requires at least python 3.5")
    sys.exit(1)

### HANDLE MODE AND PARAMETERS
mode = None
params = None
if len(sys.argv) < 2:
    usage()
    sys.exit(1)
if len(sys.argv) > 2:
    params = sys.argv[2:]
else:
    params = []
mode = sys.argv[1]

### GO TO SELECTED MODE
if mode == "build": ant_build(params)
elif mode == "run": ant_run(params)
elif mode == "clean": ant_clean(params)
elif mode == "init": ant_init(params)
elif mode == "get": ant_get(params)
elif mode == "list": ant_list(params)
elif mode == "depend": ant_depend(params)
elif mode == "state": ant_state(params)
### HELP
elif mode == "help":
    if len(params) > 1:
        error("help requires a maximum of one parameter.")
        usage("help")
        sys.exit(1)
    if len(params) == 1:
        usage(params[0], True)
    else:
        usage()
### USAGE
else:
    usage()
