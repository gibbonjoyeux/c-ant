# ANT

**ant** is a program which aims to ease C or C++ projects development,
to help developers easily build and run their projects and to create a simpler
environnement.

A **ant** project directory is organized like so:
	- ant/
	- src/
	- inc/
	- (lib/)

The **ant/** directory contains a simple "config" file and an optional "lib" file
which tells how the project should be handled.

The **src/** directory contains all the source files (.c or .cpp).
Of course, they can be organized as you want. You can even create sub
directories as **ant** will find them recursively.

The **inc/** directory contains all the header files (.h).

The **lib/** directory may contain your libraries

# Dependencies

The only ant dependency is as version of python superior to 3.5

# Commands

## ant init

Usage: `ant init [PATH] [-t template] [-b|-l]`

Init initializes or creates a new project directory.
Optionnal flags:
- `-b, --bin              The project will be created as a bin`
- `-l, --lib              The project will be created as a lib`
- `-t, --template path    The project will be created from an existant template`

By default, "ant init" creates "src/", "inc/" and "ant/" directories with a
basic "ant/config" and a simple ".gitignore" file.
`--bin` and `--lib` allows you to define the project type. (by default, the project
is a bin).

You can also use homemade templates (present at ${ANTPATH}/templates/) to
create your new project. In this way, it will copy every file from the template
directory to the project directory.
template variables:
- `${NAME}`  accessible from "ant/config" and ".gitignore", will be replaced by
    the project name.
- `${TYPE}`  accessible from "ant/config", will be replaced by the project type.

If PATH is specified, a new directory will be created and the project will be
placed here.

## ant build

Usage: `ant build [PATH]`

Build compiles project source files to build project program or library.
Sources files must be present in "src/" directory (the search is made
recursively. It means that you can have directories into "src/".).
Sources files are compiled with header files present into the "inc/" directory.
A simple compilation does not need any header file ("inc/" can be empty or
innexistant).


The "ant/" directory (optional) can contain 2 files. "config" and "lib".

"ant/config" is a the project configuration file. It's the file where you define
the project name, type, compiler, optimizations etc.
The config file is composed of a list of key/value, here's an example:
```
  name = my_program # MY PROGRAM NAME
  type = bin        # THE TYPE OF THE PROJECT, HERE, IT'S DEFINED AS A BIN
  strict = true     # ACTIVE STRICT MODE FOR COMPILATION
```
There is no required key. Here are the default values:
```
  name = a.out
  type = bin          # bin | lib
  compiler = gcc      # gcc | clang | g++ | clang++
  strict = true       # true | false
  optimization = none # none | 0 | 1 | 2 | 3 | s | fast
  flags_file =
  flags_project =
```
The value can be put on multiple lines, this can be useful for the compilation flags.

"ant/lib" is the file which defines every used external library path, here's an
example:
```
  :my_lib_string
  gitlab.com/gibbonjoyeux/my_lib_print
  ./lib/my_lib_number
  /home/gibbon/my_libs/my_lib_complex_numbers
```
Here are the different formats a path can take:
- start with ":" -> ant will look for the lib into "lib/" (in project root)
- start with "./" or "../" or "/" -> relative or absolute path
- format like "host.com/user/project" -> ant will look for the lib into the ant
  environnement sources.


if PATH is specified, **ant** will build the project present at PATH.

## ant run

Usage: `ant run [ARGUMENTS]...`

Run compiles the current project just like "ant build" does.
Once the project compiled, it will run it with given ARGUMENTS.
example: (with a.out as default program name)
```
$ ant run 1 2 3
un deux trois
```
```
$ ./a.out 1 2 3
un deux trois
```

## ant clean

Usage: `ant clean [PATH]`

Clean cleans the project directory. It removes object files (present into
the generated "obj/" directory) and the project program or library file.

## ant state

Usage: `ant state`

State prints a simple overview of the project state:
  - libraries (and if they are reachable or not)
  - includes (and if they had been modified since last compilation)
  - sources files (and if they had been modified since last compilation)

## ant get

Usage: `ant get URL`

Get get or updates project from it git URL.
The url must be formatted as followed: "host.domain/user/project", here's an
example:
```
  $ ant get gitlab.com/cactusfluo/ct_lib
```

The project will be downloaded into the **ant** environnement at ${ANTPATH}.
If the environnement variable ${ANTPATH} is not defined, **ant** will define it as
"${HOME}/ant/".
Once downloaded, the project will be found at
"${ANTPATH}/src/host.domain/user/project".
This **ant** environnement allows you to easily import your libraries from every
project on your computer.

## ant depend

Usage: `ant depend`

Depend loads the project dependencies from the "ant/lib" file. Every lib
formatted like "host.domain/user/project" will be loaded as it would be with
"ant get".

## ant tree

Usage: `ant list [-l|-t]`

List lists projects present into ${ANTPATH}/src (by default, ${HOME}/ant/src).
Optionnal flags:
- `-l, --list    Lists projects as a simple list`
- `-t, --tree    Lists projects as a tree`
